let http = require("http");

const server = http.createServer((request, response) => {
    if (request.url === "/api/users" && request.method === "GET") {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("List of all users.");
    } else if (request.url === "/api/users/0213" && request.method === "GET") {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("0213 user profile.");
    } else if (request.url === "/api/users/0213" && request.method === "PUT") {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Updated information of user 0213.");
    } else if (request.url === "/api/users/0213" && request.method === "DELETE") {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("user 0213 deleted.");
    } else if (request.url === "/api/users" && request.method === "POST") {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("New user added to users list.");
    };
});

const port = 5000;
server.listen(port);
console.log(`server running at port ${port}`);