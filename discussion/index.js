//Introduction to Postman and REST

//When a user makes a request, the url endpoint is not the only property included in that request. There is also an HTTP method that helps determine the exact operation for the server to accomplish.

//The four HTTP methods are as follows:

/*
	GET - Retrieve information about a resource
		- By default, whenever a browser loads any page, it makes a GET request

	POST - Used to create a new resource

	PUT - Used to update information about an already-existing resource

	DELETE - Used to delete an already-existing resource
*/


/*
    Scenario: You are using a website for information about music called www.coolmusicdb.com
    - The first thing you want to see is a list of all the songs on the website.
    - you go to www.coolmusicdb.com/api/songs - The browser is able to read both the URL endpoint of the address (/api/songs) that you provide as well as the method of request that you are making.
    - Since we are simply loading the page on a browser, th request that you send is automatically sent as a GET request.
    - The browser accepts your request, and create the request object with the following information (and more):
        {
            url: "api/songs",
            method: "GET"
        }
    and then shows you tis entire lst of songs.
    - Next you want to see specific information about one song, and it takes you to www.coolmusicdb.com/api/songs/1234567890
    the request for this will looks like:
        {
            url: "/api/songs/1234567890",
            method: "GET"
        }
    - Next you want to add you own original song th the database. You fill out the forms, and then press the submit button.
    - The browser makes a request that looks like this:
        {
            url: "/api/songs",
            method: "Post"
        }
    - Next You realized you entered some incorrect information about your song that you want to edit
    The browser will make a request that looks like this:
        {
            url: "/api/songs/1234567891",
            method: "PUT"
        }
    - Finally, you decide to remove your own song from the database
    The browser will make a request that looks like this:
        {
            url: "api/songs/1234567891",
            method: "DELETE"
        }
*/

let http = require("http")

const server = http.createServer(function (request, response) {
    if (request.url === "/api/products" && request.method === "GET") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Products retrieved from database.");
    } else if (request.url === "/api/products/123" && request.method === "GET") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Product 123 retrieved from database.");
    } else if (request.url === "/api/products/123" && request.method === "PUT") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Product 123 Updated.");
    } else if (request.url === "/api/products/123" && request.method === "DELETE") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Product 123 deleted.");
    } else if (request.url === "/api/products" && request.method === "POST") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("New product added to database.");
    };
});


const port = 4000;
server.listen(port);
console.log(`server running at port ${port}`);